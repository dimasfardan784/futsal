@extends('justdo')



@section('css')

@endsection

@section('content')
<div class="card">
        <div class="card-body">
          <h4 class="card-title">Data table</h4>
          <div class="row">
            <div class="col-12">
              <div class="table-responsive">
                <table id="order-listing" class="table">
                  <thead>
                    <tr>
                        <th>Nama lapangan</th>
                        <th>Jenis lapangan</th>
                        <th>Waktu</th>
                        <th>Nama team</th>
                        <th>Nama pemesan</th>
                        <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('js')

@endsection
@section('jscostume')
{{-- <p>ini contoh js </p> --}}
<script src="{{ asset('js/data-table.js') }}"></script>
@endsection
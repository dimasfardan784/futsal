@extends('justdo')



@section('css')
@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Basic form elements</h4>
            <p class="card-description">
                Basic form elements
            </p>

            <div class="form-group">
                <label for="exampleInputName1">Jenis Lapangan</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="">
            </div>
            <div class="form-group">
                <label for="exampleInputName1">Tanggal</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="">
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
            </form>
        </div>
    </div>
</div>
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Data table</h4>
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="order-listing" class="table">
                            <thead>
                                <tr>
                                    <th>Jenis lapangan</th>
                                    <th>Tanggal</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                </tr>
                                <tr>
                                   
                                </tr>
                                <tr>
                                    
                                </tr>
                                <tr>
                                   
                                </tr>
                                <tr>
                                   
                                </tr>
                                <tr>
                                    
                                </tr>
                                <tr>
                                    
                                </tr>
                                <tr>
                                   
                                </tr>
                                <tr>
                                   
                                </tr>
                                <tr>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<p>ini contoh js </p>

@section('jscostume')
{{-- <p>ini contoh js </p> --}}
<script src="{{ asset('js/data-table.js') }}"></script>

@endsection

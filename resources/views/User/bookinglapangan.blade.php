@extends('justdo')



@section('css')

@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Pembayaran</h4>
            
            <form class="forms-sample" action="{{route('masterlapangan_postView')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Nama lapangan</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder=""
                        name="Nama_lapangan">
                </div>
                <div class="form-group">
                    <label for="exampleInputName1">Jenis lapangan</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder=""
                        name="jenis_lapangan">
                </div>
                <div class="form-group">
                    <label for="exampleInputName1">Waktu</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder=""
                        name="kode_lapangan">
                </div>

                <div class="form-group">
                    <label for="exampleInputName1">Nama team</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder=""
                        name="Nama_team">
                </div>
                <div class="form-group">
                    <label for="exampleInputName1">Nama pemesan</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder=""
                        name="Nama_pemesanan">
                </div>

                <button type="submit" class="btn btn-primary mr-2">Lanjut Pembayaran</button>

            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection

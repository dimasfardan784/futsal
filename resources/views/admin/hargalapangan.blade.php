@extends('justdo')



@section('css')
{{-- <p>ini contoh css </p>? --}}
@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"></h4>
            <p class="card-description">
                Harga Lapangan
            </p>
            <form class="forms-sample">
                <div class="form-group">
                    <label for="exampleInputName1">Kode lapangan</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder="kode lapangan">
                </div>
                <form class="forms-sample">
                    <div class="form-group">
                        <label for="exampleInputName1">Kode jenis lapangan</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="jenis lapangan">
                    </div>
                    <form class="forms-sample">
                        <div class="form-group">
                            <label for="exampleInputName1">Harga / jam</label>
                            <input type="text" class="form-control" id="exampleInputName1" placeholder="harga">
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                    </form>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data table</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                    <tr>
                                        <th>Kode lapangan</th>
                                        <th>Jenis lapangan</th>
                                        <th>Harga lapangan</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <label class="badge badge-danger">Pending</label>
                                    </td>
                                    <td>
                                        <button class="btn btn-outline-primary">View</button>
                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('jscostume')
<script>
$( document ).ready(function() {
// console.log( "ready!" );

var oTable = $('#order-listing').DataTable({
processing: true,
serverSide: true,
ajax: {
    url: '{{ route("hargajenislapangan_getList") }}'
},
columns: [
  // console.log(data),
// {data: 'id', name: 'id'},
{data: 'kode_lapangan', name: 'kode_lapangan'},
{data: 'kode_jenis_lapangan', name: 'kode_jenis_lapangan'},
{data: 'harga_lapangan', name: 'harga_lapangan'},

{ data: 'id',
render: function(data, type, row)
    {
    // console.log(data);
    let buttonEdit = '<button type="button" class="btn-sm btn-inverse-primary mr-2" data-toggle="modal" data-target="#exampleModal" onclick="buttonEdit(\''+data+'\');"><i class="fa fa-edit"></i></button>';
    let buttonHapus = '<button type="button" class="btn-sm btn-inverse-danger" onclick="buttonDelete(\''+data+'\');" ><i class="fa fa-trash-o"></i></button>';

    return buttonEdit+buttonHapus;
    } 
}
],
});
});
$('#formNew').on('submit', function (e) { console.log("submit di tekan");
    // $('#closeModalTambah').click();
        //buat preevent untuk ajax event
        e.preventDefault();
        //untuk ajax setup kirim token agar bisa akses method post
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        }
    });
        //proses kirim data ke Controller
    $.ajax({
        //type yg akan di kirim => ada get atau post
        type: "POST",
        //url ini di sesuaikan dengan routing yg udah d bikin
        url: "",
        //untuk data ini kalo semua isi form akan d kirimkan k controller amka menggunakan form serialize
        data: $(this).serialize(),
        //success cuma buat method ajax ajax , yg intinya di pake sh function(response) nya itu sesuai dengan yg kita kirimkan dari controller
        success: function(response) {
            if (response.status == 200){
               
                $('#formNew').trigger("reset");
                oTable.ajax.reload();
                $.toast({
                    heading: 'Success',
                    text: response.message,
                    showHideTransition: 'slide',
                    icon: 'success',
                    loaderBg: '#f96868',
                    position: 'top-right'
                })
            }else{
                $.toast({
                    heading: 'Danger',
                    text: response.message,
                    showHideTransition: 'slide',
                    icon: 'error',
                    loaderBg: '#f2a654',
                    position: 'top-right'
                })
            }
        } //end response success       ,
        // error: function (response){
        //     // JSON.parse()
        //     $.toast({
        //             heading: 'Danger',
        //             text: response.responseJSON.errors,
        //             showHideTransition: 'slide',
        //             icon: 'error',
        //             loaderBg: '#f2a654',
        //             position: 'top-right'
        //         })
        // }
        
    });
        // return true;
});

function buttonEdit1(idx){
  console.log(idx);
}

function buttonDelete(idx){
  console.log(idx);
}

</script>
@endsection


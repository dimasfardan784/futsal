@extends('justdo')



@section('css')

@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Basic form elements</h4>
            <p class="card-description">
              Basic form elements
            </p>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <form class="forms-sample" action="{{route('masterlapangan_Postupdate' , ['id'=>$lapangan->id])}}" method="POST">
              @csrf
              <div class="form-group">
                <label for="exampleInputName1">Kode lapangan</label>
              <input type="text" class="form-control" id="exampleInputName1" placeholder="Name" name="kode_lapangan" value="{{$lapangan->kode_lapangan}}"> 
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Nama lapangan</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="Name" name="nama lapangan" value="{{$lapangan->nama_lapangan}}">
              </div>
  
              <button type="submit" class="btn btn-primary mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button>
            </form>
          </div>
        </div>
      </div>
@endsection

@section('js')

@endsection

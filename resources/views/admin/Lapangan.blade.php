@extends('justdo')



@section('css')
{{-- <p>ini contoh css </p>? --}}
@endsection

@section('content')
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Basic form elements</h4>
          <p class="card-description">
            Basic form elements
          </p>
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
          <form class="forms-sample" action="{{route('masterlapangan_postView')}}" method="POST">
            @csrf
            <div class="form-group">
              <label for="exampleInputName1">Kode lapangan</label>
              <input type="text" class="form-control" id="exampleInputName1" placeholder="Name" name="kode_lapangan">
            </div>
            <div class="form-group">
              <label for="exampleInputName1">Nama lapangan</label>
              <input type="text" class="form-control" id="exampleInputName1" placeholder="Name" name="nama lapangan">
            </div>

            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
          </form>
        </div>
      </div>
    </div>

    <div class="col-12 grid-margin stretch-card">
      {{-- <div class="content-wrapper"> --}}
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Data table</h4>
            <div class="row">
              <div class="col-12">
                <div class="table-responsive">
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                          <th>Kode lapangan</th>
                          <th>Nama lapangan</th>
                          <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($lapangan as $row)
                        <tr>
                          <td>{{ $row->kode_lapangan }}</td>
                          <td>{{ $row->nama_lapangan }}</td>
                        <td><a href="{{ route('masterlapangan_getdelete', ['id' => $row->id ]) }}">Delete</a>
                        <a href="{{ route('masterlapangan_getupdate', ['id' => $row->id ]) }}">Update</a></td>
                      </tr>

                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        {{-- </div> --}}
      </div>
@endsection

@section('js')
{{-- <p>ini contoh js </p> --}}
{{-- <script src="../../../../js/data-table.js"></script> --}}
@endsection

@section('jscostume')
{{-- <p>ini contoh js </p> --}}
<script src="{{ asset('js/data-table.js') }}"></script>

@endsection
@extends('justdo')



@section('css')
{{-- <p>ini contoh css </p>? --}}
@endsection

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">

            <p class="card-description">
                FORM USER
            </p>
            <form class="forms-sample">
                <div class="form-group">
                    <label for="exampleInputName1">Name</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Alamat</label>
                    <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputName1">No.Telpon</label>
                    <input type="text" class="form-control" id="exampleInputName1" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail3">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword4">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
                </div>
                <div class="form-group">
                    <label>File upload</label>
                    <input type="file" name="img[]" class="file-upload-default">
                    <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputCity1">City</label>
                    <input type="text" class="form-control" id="exampleInputCity1" placeholder="Location">
                </div>

                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
            </form>
            <div class="col-12 grid-margin stretch-card">
                {{-- <div class="content-wrapper"> --}}
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data table</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table id="order-listing" class="table">
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                
                                                <th>No.Telpon</th>
                                                <th>Email</th>

                                                <th>File Gambar</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
        @endsection

        @section('js')
        {{-- <p>ini contoh js </p> --}}

        <script src="{{ asset('js/file-upload.js') }}"></script>
        @endsection

        @section('jscostume')
            <script>
            $( document ).ready(function() {
  // console.log( "ready!" );

  var oTable = $('#order-listing').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("User_getList") }}'
            },
            columns: [
              // console.log(data),
            // {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'no_tlp', name: 'no_tlp'},
            {data: 'foto', name: 'foto'},
            { data: 'id',
            render: function(data, type, row)
                {
                // console.log(data);
                let buttonEdit = '<button type="button" class="btn-sm btn-inverse-primary mr-2" data-toggle="modal" data-target="#exampleModal" onclick="buttonEdit(\''+data+'\');"><i class="fa fa-edit"></i></button>';
                let buttonHapus = '<button type="button" class="btn-sm btn-inverse-danger" onclick="buttonDelete(\''+data+'\');" ><i class="fa fa-trash-o"></i></button>';

                return buttonEdit+buttonHapus;
                } 
            }
        ],
      });
});
            
            </script>
        @endsection

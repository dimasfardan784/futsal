@extends('justdo')



@section('css')
{{-- <p>ini contoh css </p>? --}}
@endsection

@section('content')
    <div class="col-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Olahraga</h4>
                
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

               
      <form class="forms-sample" method="POST" action="#" id="formNew">
                @csrf
                <input type="hidden" id="id" name="id" class="clear">
                  <div class="form-group">
                    <label for="exampleInputName1">Kode olahraga</label>
                    <input type="text" class="form-control" id="kode" placeholder="Name" name="kode_olahraga">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputName1">Nama olahraga</label>
                    <input type="text" class="form-control" id="name" placeholder="Name" name="nama_olahraga">
                  </div>
                  
                  <button type="submit" class="btn btn-primary mr-2">Submit</button>
                  <a href="#" class="btn btn-light clear">Clear</a>
                  {{-- <button class="btn btn-light clear">Clear</button> --}}
                </form>
                <div class="card">
                  <div class="card-body">
                      <h4 class="card-title">Data table</h4>
                      <div class="row">
                          <div class="col-12">
                              <div class="table-responsive">
                                  <table id="order-listing" class="table">
                                      <thead>
                                          <tr>
                                              <th>Kode Olahraga</th>
                                              <th>Nama Olahraga</th>
                                              <th>Actions</th>
                                              
                                          </tr>
                                      </thead>
                                      <tbody>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
              </div>
            </div>
          </div>

@endsection

@section('js')
{{-- <p>ini contoh js </p> --}}
@endsection


@section('jscostume')
{{-- <p>ini contoh js </p> --}}
<script>
$( ".clear" ).click(function() {
    $("#id").val("");
    $('#formNew').trigger("reset");
});

var oTable;
$( document ).ready(function() {
  // console.log( "ready!" );

  oTable = $('#order-listing').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("masterolahraga_getList") }}'
            },
            columns: [
            //  console.log(type),
            // {data: 'id', name: 'id'},
            {data: 'kode_olahraga', name: 'kode_olahraga'},
            {data: 'nama_olahraga', name: 'nama_olahraga'},
            {data: 'id',
            render: function(data, type, row)
                {
                // console.log(data);
                let buttonEdit = '<button type="button" class="btn-sm btn-inverse-primary mr-2" data-toggle="modal" data-target="#exampleModal" onclick="buttonEdit(\''+data+'\');"><i class="fa fa-edit"></i></button>';
                let buttonHapus = '<button type="button" class="btn-sm btn-inverse-danger" onclick="buttonDelete(\''+data+'\');" ><i class="fa fa-trash-o"></i></button>';

                return buttonEdit+buttonHapus;
                } 
            }
        ],
      });
});

$('#formNew').on('submit', function (e) {
  // console.log("submit di tekan");
    // $('#closeModalTambah').click();
        //buat preevent untuk ajax event
        e.preventDefault();
        //untuk ajax setup kirim token agar bisa akses method post
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        }
    });
        //proses kirim data ke Controller
    $.ajax({
        //type yg akan di kirim => ada get atau post
        type: "POST",
        //url ini di sesuaikan dengan routing yg udah d bikin
        url: "{{ route ('masterolahraga_postView') }}",
        //untuk data ini kalo semua isi form akan d kirimkan k controller amka menggunakan form serialize
        data: $(this).serialize(),
        //success cuma buat method ajax ajax , yg intinya di pake sh function(response) nya itu sesuai dengan yg kita kirimkan dari controller
        success: function(response) {
            if (response.status == 200){
                
                $('#formNew').trigger("reset");
                $("#id").val("");
                oTable.ajax.reload();
                $.toast({
                    heading: 'Success',
                    text: response.message,
                    showHideTransition: 'slide',
                    icon: 'success',
                    loaderBg: '#f96868',
                    position: 'top-right'
                })
            }else{
                $.toast({
                    heading: 'Danger',
                    text: response.message,
                    showHideTransition: 'slide',
                    icon: 'error',
                    loaderBg: '#f2a654',
                    position: 'top-right'
                })
            }
        } //end response success       ,
        // error: function (response){
        //     // JSON.parse()
        //     $.toast({
        //             heading: 'Danger',
        //             text: response.responseJSON.errors,
        //             showHideTransition: 'slide',
        //             icon: 'error',
        //             loaderBg: '#f2a654',
        //             position: 'top-right'
        //         })
        // }
        
    });
        // return true;
});
function buttonEdit(idx){
  //console.log(idx);
  // console.log(idx);
  // e.preventDefault();
        //untuk ajax setup kirim token agar bisa akses method post
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        }
    });
        //proses kirim data ke Controller
    $.ajax({
        //type yg akan di kirim => ada get atau post
        type: "GET",
        //url ini di sesuaikan dengan routing yg udah d bikin
        url: "{{ route ('masterolahraga_getupdate') }}",
        //untuk data ini kalo semua isi form akan d kirimkan k controller amka menggunakan form serialize
        data: {id:idx},
        //success cuma buat method ajax ajax , yg intinya di pake sh function(response) nya itu sesuai dengan yg kita kirimkan dari controller
        success: function(response) {
            if (response){
                $("#id").val(response.id);
                $("#kode").val(response.kode_olahraga);
                $("#name").val(response.nama_olahraga);
           
            }
        } //end response success       ,
        
        
    });
}

function buttonDelete(idx){
  // console.log(idx);
  // e.preventDefault();
        //untuk ajax setup kirim token agar bisa akses method post
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("input[name='_token']").val()
        }
    });
        //proses kirim data ke Controller
    $.ajax({
        //type yg akan di kirim => ada get atau post
        type: "POST",
        //url ini di sesuaikan dengan routing yg udah d bikin
        url: "{{ route ('masterolahraga_postdelete') }}",
        //untuk data ini kalo semua isi form akan d kirimkan k controller amka menggunakan form serialize
        data: {id:idx},
        //success cuma buat method ajax ajax , yg intinya di pake sh function(response) nya itu sesuai dengan yg kita kirimkan dari controller
        success: function(response) {
            if (response.status == 200){
                
                // $('#formNew').trigger("reset");
                oTable.ajax.reload();
                $.toast({
                    heading: 'Success',
                    text: response.message,
                    showHideTransition: 'slide',
                    icon: 'success',
                    loaderBg: '#f96868',
                    position: 'top-right'
                })
            }else{
                $.toast({
                    heading: 'Danger',
                    text: response.message,
                    showHideTransition: 'slide',
                    icon: 'error',
                    loaderBg: '#f2a654',
                    position: 'top-right'
                })
            }
        } //end response success       ,
        // error: function (response){
        //     // JSON.parse()
        //     $.toast({
        //             heading: 'Danger',
        //             text: response.responseJSON.errors,
        //             showHideTransition: 'slide',
        //             icon: 'error',
        //             loaderBg: '#f2a654',
        //             position: 'top-right'
        //         })
        // }
        
    });

}


</Script>
@endsection
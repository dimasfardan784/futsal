<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHargajenislapanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hargajenislapangan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kode_jenis_lapangan')->unsigned();
            $table->bigInteger('kode_lapangan')->unsigned();
            $table->bigInteger('harga_lapangan');
            
            $table->timestamps();
            $table->foreign('kode_lapangan')->references('id')->on('lapangan');
            $table->foreign('kode_jenis_lapangan')->references('id')->on('jenislapangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hargajenislapangan');
    }
}

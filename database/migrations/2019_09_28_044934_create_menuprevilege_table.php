<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuprevilegeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuprevilege', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('Id_usermenu')->unsigned();
            $table->bigInteger('Id_previlege')->unsigned();
            $table->timestamps();
            $table->foreign('Id_usermenu')->references('id')->on('usermenu');
            $table->foreign('Id_previlege')->references('id')->on('previlege');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menuprevilege');
    }
}

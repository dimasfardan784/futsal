<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenislapanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenislapangan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kode_olahraga')->unsigned();
            $table->string('kode_jenis_lapangan', 30);
            $table->string('nama_jenis_lapangan', 30);
            
            $table->timestamps();
            $table->foreign('kode_olahraga')->references('id')->on('olahraga');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenislapangan');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idhargajenislapangan')->unsigned();
            $table->date('date');
            $table->string('time_start', 30);
            $table->integer('durasi');
            $table->string('status');
            $table->biginteger('iduser')->unsigned();
            $table->string('namateam', 30);
            $table->timestamps();
            $table->foreign('idhargajenislapangan')->references('id')->on('hargajenislapangan');
            $table->foreign('iduser')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}

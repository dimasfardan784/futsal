<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingbayarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookingbayar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idbookingbayar')->unsigned();
            $table->string('Uploadgambar');
            $table->string('nomorpembayaran');
            $table->bigInteger('totalharusdibayar');
            $table->bigInteger('sisabayar');
            $table->boolean('lunas');
            $table->timestamps();
            $table->foreign('idbookingbayar')->references('id')->on('booking');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookingbayar');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsermenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usermenu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('Id_user')->unsigned();
            $table->bigInteger('Id_menu')->unsigned();
            $table->timestamps();
            $table->foreign('Id_menu')->references('id')->on('menu');
            $table->foreign('Id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usermenu');
    }
}

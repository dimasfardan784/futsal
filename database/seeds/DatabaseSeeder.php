<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(OlahragaSeeder::class);
         $this->call(LapanganSeeder::class);
         
         $this->call(JenislapanganSeeder::class);
         $this->call(UserSeeder::class);
        //  $this->call(HargajenislapanganSeeder::class);
    }
}

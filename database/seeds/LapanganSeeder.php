<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Lapangan;

class LapanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // dd('test');
        for ($i=0; $i < 10; $i++) { 
            # code...
        //     DB::table('lapangan')->insert([
        //         'kode_lapangan' => "Kode".$i,
        //         'nama_lapangan' => "Nama".$i,
        //         // 'nama_lapangan' => Str::random(10)."___".$i,
               
        //    ]);
            $lapangan                   = new Lapangan;
            $lapangan->kode_lapangan   ='kode_'.$i;
            $lapangan->nama_lapangan   ='nama_'.$i;
            $lapangan->save();
        }
        
    }
}

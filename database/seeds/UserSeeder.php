<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        //
        // DB::table('olahraga')->insert([
        //     'kode_olahraga' => Str::random(10),
        //     'nama_olahraga' => Str::random(10),
            
        // ]);
        for ($i=1; $i < 10; $i++) { 
            # code...
            $user                  = new User;
            $user->name      ='name'.$i;
            $user->email     ='email'.$i.'@gmail.com';
            $user->no_tlp    ='no_tlp'.$i;
            $user->foto     ='foto'.$i;
            $user->password  =bcrypt("12345678");
            $user->save();

        } 
    }
}

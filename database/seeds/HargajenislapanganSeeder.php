<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\HargajenisLapangan;
class HargajenislapanganSeeder extends Seeder
{
   
    
    public function run()
    {
        
        for ($i=1; $i < 5; $i++) { 
            # code...
            $user                     = new Hargajenislapangan;
            $user->kode_lapangan      =$i;
            $user->kode_jenis_lapangan=$i;
            $user->harga_lapangan  =$i;
            
            $user->save();

        } 
    }
    
}

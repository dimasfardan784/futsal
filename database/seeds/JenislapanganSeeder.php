<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\JenisLapangan;

class JenislapanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('olahraga')->insert([
        //     'kode_olahraga' => Str::random(10),
        //     'nama_olahraga' => Str::random(10),
            
        // ]);
        // dd('tes');
        for ($i=1; $i < 8; $i++) { 
            # code...
            $user                     = new Jenislapangan;
            $user->kode_olahraga      =$i;
            $user->kode_jenis_lapangan=$i;
            $user->nama_jenis_lapangan  ='nama_jenislapangan'.$i;
            
            $user->save();

        } 
    }
}

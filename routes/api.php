<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/olahraga1/list', 'OlahragaController@Olahraga_getlist')->name('masterolahraga_getList');
Route::get('/olahraga1/update/', 'OlahragaController@olahraga_getupdate')->name('masterolahraga_getupdate');
Route::post('/olahraga1/post', 'OlahragaController@Olahraga_POSTindex')->name('masterolahraga_postView');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

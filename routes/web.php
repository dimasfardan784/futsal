<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::middleware('auth')->group(function() {
   
// });


Route::group(['middleware' => 'auth'],function() {
    Route::get('/olahraga1', 'OlahragaController@Olahraga_getindex')->name('masterolahraga_getView');
    // Route::get('/lapangan', 'lapanganController@lapangan_getindex')->name('masterlapangan_getView');
});


Route::get('/', function () {
    return view('welcome');
});
Route::get('/dashboard', function () {
    return view('admin.dashboard');
});

Route::get('/temple', function () {
    return view('tamplateBoostrap');
});
Route::get('/just', function () {
    return view('justdo');
});
Route::get('/load', function () {
    return view('user.loadlapangan');
});
Route::get('/booking', function () {
    return view('user.bookinglapangan');
});
Route::get('/pembayaran', function () {
    return view('user.pembayaranlapangan');
});
Route::get('/historylapangan', function () {
    return view('user.historybookinglapangan');
});
Route::get('viewlapangan', function () {
    return view('user.viewbookinglapangan');
});
Route::get('/historylapangan', function () {
    return view('admin.historybookinglapangan');
});
Route::get('/tabelolahraga', function () {
    return view('admin.tabelolahraga');
});
Route::get('/update', function () {
    return view('admin.lapanganupdate');
});

Route::get('/hargalapangan', function () {
    return view('admin.hargalapangan');
});
Route::get('/jenislapangan', function () {
    return view('admin.Jenislapangan');
})->name('masterjenislapangan_getView');
Route::get('/olahraga', function () {
    return view('admin.Olahraga');
})->name('masterolahraga_getView');
Route::get('/lapangan', function () {
    return view('admin.lapangan');
})->name('masterlapangan_getView');
Route::get('/waktu', function () {
    return view('admin.masterwaktu');
})->name('masterwaktu_getView');
Route::get('/hargajenislapangan', function () {
    return view('admin.hargalapangan');
})->name('masterhargalapangan_getView');
Route::get('/user', function () {
    return view('admin.User');
})->name('masteruser_getView');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/olahraga1', 'OlahragaController@Olahraga_getindex')->name('masterolahraga_getView');
Route::get('/olahraga1/list', 'OlahragaController@Olahraga_getlist')->name('masterolahraga_getList');
Route::post('/olahraga1/post', 'OlahragaController@Olahraga_POSTindex')->name('masterolahraga_postView');
Route::post('/olahraga1/delete', 'OlahragaController@olahraga_postdelete')->name('masterolahraga_postdelete');
Route::get('/olahraga1/update/', 'OlahragaController@olahraga_getupdate')->name('masterolahraga_getupdate');

 Route::get('/lapangan', 'lapanganController@lapangan_getindex')->name('masterlapangan_getView');
Route::post('/lapangan/post', 'lapanganController@lapangan_POSTindex')->name('masterlapangan_postView');
Route::get('/lapangan/delete/{id}', 'lapanganController@lapangan_getdelete')->name('masterlapangan_getdelete');
Route::get('/lapangan/update/{id}', 'lapanganController@lapangan_getupdate')->name('masterlapangan_getupdate');
Route::post('/lapangan/Postupdate/{id}', 'lapanganController@lapangan_Postupdate')->name('masterlapangan_Postupdate');

Route::post('/jenislapangan/delete', 'JenislapanganController@jenislapangan_postdelete')->name('jenislapangan_postdelete');
Route::get('/jenislapangan/update/', 'JenislapanganaController@jenislapangan_getupdate')->name('jenislapangan_getupdate');
Route::post('/jenislapangan/post', 'JenislapanganController@jenislapangan_POSTindex')->name('masterjenislapangan_postView');

Route::get('/hargajenislapangan/update/', 'HargajenislapanganController@hargajenislapangan_getupdate')->name('hargalapangan_getupdate');
Route::post('/hargajenislapangan/delete', 'Hargajenislapangan@ohargajenislapangan_postdelete')->name('hargalapangan_postdelete');

Route::get('/user/list', 'UserController@User_getlist')->name('User_getList');
Route::get('/jenislapangan/list', 'jenislapanganController@Jenislapangan_getlist')->name('Jenislapangan_getList');
Route::get('/hargajenislapangan/list', 'HargajenislapanganController@hargajenislapangan_getlist')->name('hargajenislapangan_getList');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');










Route::get('/list-stock', function() {
    $begin = memory_get_usage();
    foreach (DB::table('olahraga')->get() as $product) {
    //   if ( $product->stock > 20 ) {
        echo $product->kode_olahraga . ': '. $product->kode_olahraga . '<br>';
    //   }
    }
    echo 'Total memory usage : '. (memory_get_usage() - $begin);
  });


Route::get('/list-stock-chunk', function() {
    $begin = memory_get_usage();
    // $data = App\Models\Olahraga::chunk(10000, function($users) {
    //     foreach ($users as $user) {
    //         // if ( $product->stock > 20 ) {
    //             echo $user->kode_olahraga . ': '. $user->kode_olahraga . '<br>';
    //         // }
    //     }
    // });
    $minutes = 1;
    $value = Cache::remember('olahraga', $minutes, function () {
        DB::table('olahraga')->select('kode_olahraga')->orderBy('id')->chunk(10000, function($users) {
            foreach ($users as $user) {
                // if ( $product->stock > 20 ) {
                    echo $user->kode_olahraga . ': '. $user->kode_olahraga . '<br>';
                // }
            }
        });
    });
    echo 'Total memory usage : ' . (memory_get_usage() - $begin);
});

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Previlege extends Model
{
    protected $table = 'menu';
    public $fillable = ['nama_previlege'];
    public $timestamps = true;
    public function mmenuprevilege()
    {
        return $this->hasMany('App\Models\Menuprevilege');
    }
}

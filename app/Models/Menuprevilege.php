<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menuprevilege extends Model
{
    protected $table = 'menuprevilege';
    public $fillable = ['Id_usermenu','Id_previlege'];
    public $timestamps = true; 
    public function mprivilege()
    {
        return $this->belongsTo('App\Models\Privilege');
    }
    public function musermenu()
    {
        return $this->belongsTo('App\Models\Usermenu');
    }
}

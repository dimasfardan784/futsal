<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hargajenislapangan extends Model
{
    protected $table = 'hargajenislapangan';
    public $fillable = [
        'kode_jenis_lapangan',
        'kode_lapangan',
        'harga_lapangan'
    ];
    public $timestamps = true;
    public function mjenislapangan()
    {
        return $this->belongsTo('App\Models\Jenislapangan');

    }
    public function mlapangan()
    {
        return $this->belongsTo('App\Models\Lapangan','kode_lapangan','id' );
    }
    public function mbooking()
    {
        return $this->hasMany('App\Models\Booking');
    }
}

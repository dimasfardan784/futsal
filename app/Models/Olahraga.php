<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Olahraga extends Model
{
    protected $table = 'olahraga';
    public $fillable = ['kode_olahraga','nama_olahraga'];
    public $timestamps = true;

    public function jenislapangan()
    {
        return $this->hasMany('App\Models\Jenislapangan');
    }
}

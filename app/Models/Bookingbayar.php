<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookingbayar extends Model
{
    protected $table = 'bookingbayar';
    public $fillable = ['idbookingbayar','Uploadgambar','nomorpembayaran','totalharusdibayar','sisabayar','lunas'];
    public $timestamps = true;
    public function mbooking()
    {
        return $this->belongsTo('App\Models\Booking');

    }
}

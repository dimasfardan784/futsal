<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Waktu extends Model
{
    protected $table = 'waktu';
    public $fillable = ['waktu','hari'];
    public $timestamps = true;
}

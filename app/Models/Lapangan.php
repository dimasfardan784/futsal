<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lapangan extends Model
{
    protected $table = 'lapangan';
    public $fillable = ['kode_lapangan','nama_lapangan'];
    public $timestamps = true;

    public function mLapangan()
    {
        return $this->hasMany('App\Models\Lapangan');
    }
}

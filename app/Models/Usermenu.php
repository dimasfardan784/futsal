<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usermenu extends Model
{
    protected $table = 'menu';
    public $fillable = ['Id_user','Id_menu'];
    public $timestamps = true;
    public function mmenuprevilege()
    {
        return $this->hasMany('App\Models\Menuprivilege');
    }
    public function mmenu()
    {
        return $this->belongsTo('App\Models\Menu');
    }
    public function muser()
    {
        return $this->belongsTo('App\Models\User');
    }
}

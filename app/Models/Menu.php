<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    public $fillable = ['nama_menu'];
    public $timestamps = true;
    public function musermenu()
    {
        return $this->hasMany('App\Models\Usermenu');
    }
}

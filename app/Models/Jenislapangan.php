<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jenislapangan extends Model
{
    protected $table = 'jenislapangan';
    public $fillable = ['kode_olahraga','kode_jenis_lapangan','nama_jenis_lapangan'];
    public $timestamps = true;

    public function molahraga()
    {
        return $this->belongsTo('App\Models\Olahraga', 'kode_olahraga', 'id');
    }
    public function mhargajenislapangan()
    {
        return $this->hasMany('App\Models\Hargajenislapangan','kode_jenis_lapangan', 'id');
    }
}

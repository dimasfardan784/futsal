<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'booking';
    public $fillable = ['idhargajenislapangan','date','time_start','durasi','status','iduser','namateam'];
    public $timestamps = true;
    public function mhargajenislapangan()
    {
        return $this->belongsTo('App\Models\Hargajenislapangan');

    }
    public function muser()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function mbookingbayar()
    {
        return $this->hasMany('App\Models\Bookingbayar');
    }
}

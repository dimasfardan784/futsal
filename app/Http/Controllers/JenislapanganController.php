<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jenislapangan;
use Yajra\Datatables\Datatables;
class JenislapanganController extends Controller
{
    public function Jenislapangan_getindex(){
        return view('admin.Jenislapangan');
    }
    public function Jenislapangan_getlist(){
        $res = Jenislapangan::
       
        // query()
        get()
        ->map(function($key){
            return [
                'id' => $key->id,
                'kode_olahraga' =>$key->molahraga->nama_olahraga,
                'kode_jenis_lapangan' =>$key->kode_jenis_lapangan,
                'nama_jenis_lapangan' =>$key->nama_jenis_lapangan,

            ];
        });
        return Datatables::of($res)->make(true);
    }
    public function jenislapangan_POSTindex(Request $abc){
        // dd('test');
        dd($abc->all());
        $jenislapangan=new jenislapangan;
        $jenislapangan->kode_olahraga=$abc->input('kode_olahraga');
        $jenislapangan->kode_jenis_lapangan=$abc->input('kode_jenis_lapangan');
        $jenislapangan->nama_jenis_lapangan=$abc->input('nama_jenis_lapangan');
        $lapangan->save();
        // return view('admin.Olahraga');
        return response()->json(['status'=>200, 'message'=>"Data have been inserted"]);
        
    }
    public function jenislapangan_postdelete(){
        $id = request()->input('id');
        // dd($id);
        $jenislapangan = Jenislapangan::find($id);
        $jenislapangan->delete(); 
        // dd($lapangan);
        return response()->json(['status'=>200, 'message'=>"Data have been delete"]);
    }
    public function jenislapangan_getupdate(){
        $id = request()->input('id');
      
        $jenislapangan = Jenislapangan::find($id);
        // dd($olahraga);
        // $lapangan->delete(); 
        // dd($lapangan);
        return response()->json($jenislapangan);
    }

  

    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Olahraga;
// use Validator;
use App\Http\Requests\StoreOlahraga;
use Yajra\Datatables\Datatables;
use DB;


class OlahragaController extends Controller
{
    //
    public function Olahraga_getindex(){
        return view('admin.Olahraga');
    }
    public function Olahraga_getlist(){
        // $res = Olahraga::
        // // take(1000)->
        // query();
        // return Datatables::of($res)->make(true);


        $begin = memory_get_usage();
        // // $res = Olahraga::
        // // // take(100000)->
        // // query();

        // $res = DB::table('olahraga')->select('kode_olahraga', 'nama_olahraga')->chunk();
        // $res = DB::table('olahraga')->orderBy('id')->chunk(1000, function($products) {
        //     foreach ($products as $product) {
        //         dd(memory_get_usage());
        //     //   if ( $product->kode_olahraga == 'kode_array_0' ) {
        //         // echo $product->kode_olahraga . ' : ' . $product->kode_olahraga . '<br>';
        //     //   }
        //     }
            
        //   });
        $res = DB::table('olahraga');
        // $res = Olahraga::query();
        //   dd( 'Total memory usage : ' . (memory_get_usage() - $begin));
        //   dd($res);
        
        return Datatables::of($res)->make(true);
    }

    public function Olahraga_POSTindex(Request $abc){ //StoreOlahraga
        // dd($abc->input('kode_olahraga'));
        // $validatedData = $abc->validate([
        //     'kode_olahraga' => 'required|unique:olahraga|max:255',
        //     'nama_olahraga' => 'required|unique:olahraga|max:255',
        // ]);
        // dd('test');
        // dd($abc->all());
        if ($abc->id == null ){
        $olahraga=new Olahraga;
        $olahraga->kode_olahraga=$abc->input('kode_olahraga');
        $olahraga->nama_olahraga=$abc->input('nama_olahraga');
        $olahraga->save();
        // return view('admin.Olahraga');

        return response()->json(['status'=>200, 'message'=>"Data have been inserted"]);
        }else{
        $olahraga= Olahraga::find($abc->id);
        $olahraga->kode_olahraga=$abc->input('kode_olahraga');
        $olahraga->nama_olahraga=$abc->input('nama_olahraga');
        $olahraga->save();
        // return view('admin.Olahraga');

        return response()->json(['status'=>200, 'message'=>"Data have been updated"]);
    }

}
    
    public function olahraga_postdelete(){
        $id = request()->input('id');
        // dd($id);
        $olahraga = Olahraga::find($id);
        $olahraga->delete(); 
        // dd($lapangan);
        return response()->json(['status'=>200, 'message'=>"Data have been delete"]);
    }
    public function olahraga_getupdate(){
        $id = request()->input('id');
      
        $olahraga = Olahraga::find($id);
        // dd($olahraga);
        // $lapangan->delete(); 
        // dd($lapangan);
        return response()->json($olahraga);
    }
}


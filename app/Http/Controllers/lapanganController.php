<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lapangan;
use App\Http\Requests\Storelapangan;
class lapanganController extends Controller
{
    //
    public function lapangan_getindex(){
        $lapangan = Lapangan::all();
        // dd($lapangan);
        return view('admin/Lapangan', compact('lapangan'));
    }
    public function lapangan_POSTindex(Storelapangan $abc){
        // dd('test');
        // dd($abc->all());
        $lapangan=new Lapangan;
        $lapangan->kode_lapangan=$abc->input('kode_lapangan');
        $lapangan->nama_lapangan=$abc->input('nama_lapangan');
        $lapangan->save();
        // return view('admin.Olahraga');
        return redirect()->route('masterlapangan_getView');
        
    }
    public function lapangan_gettabel(){
        $lapangan = Lapangan::get();
        // dd($lapangan);
        return view('admin.lapangan',['lapangan' => $lapangan ]);

    }
    public function lapangan_getdelete(Request $request, $id){
        // dd($id);
        $lapangan = Lapangan::find($id);
        $lapangan->delete(); 
        // dd($lapangan);
        return redirect()->route('masterlapangan_getView');

    }
    public function lapangan_getupdate($id){
        // dd($id);
        $lapangan = Lapangan::find($id);
        // $lapangan->delete(); 
        // dd($lapangan);
        return view('admin.lapanganupdate',compact('lapangan'));
    }
    public function lapangan_Postupdate( Request $abc,$id){
        // dd('test');
        // dd($abc->all());
        $lapangan=Lapangan::find($id);
        $lapangan->kode_lapangan=$abc->input('kode_lapangan');
        $lapangan->nama_lapangan=$abc->input('nama_lapangan');
        $lapangan->save();
        // return view('admin.Olahraga');
        return redirect()->route('masterlapangan_getView');
        
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Storelapangan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'kode_lapangan' => 'required|unique:lapangan|min:3|max:255',
                'nama_lapangan' => 'required|unique:lapangan|max:255',
        ];
    }
    public function messages()
    {
        return [
            'kode_lapangan.required' => 'Masukkan kode lapangan',
            'kode_lapangan.min' => 'karakter tidak kurang dari 3 karakter',
            'kode_lapangan.unique' => 'kode olahraga tidak boleh sama',

            'nama_lapangan.required' => 'Masukkan nama lapangan',
            'nama_lapangan.max' => 'jumlah karakter tidak boleh lebih 255',
            'nama_lapangan.unique' => 'Nama tidak boleh sama',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOlahraga extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_olahraga' => 'required|unique:olahraga|min:3|max:255',
            'nama_olahraga' => 'required|unique:olahraga|max:255',
        ];
    }
    public function messages()
    {
        return [
            'kode_olahraga.required' => 'Masukkan kode olahraga',
            'kode_olahraga.min' => 'karakter tidak kurang dari 3 karakter',
            'kode_olahraga.unique' => 'kode olahraga tidak boleh sama',

            'nama_olahraga.required' => 'Masukkan nama olahraga',
            'nama_olahraga.max' => 'jumlah karakter tidak boleh lebih 255',
            'nama_olahraga.unique' => 'Nama tidak boleh sama',
        ];
    }
}
